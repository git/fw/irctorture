/*
 * This file is in the public domain.
 */
#include <stdio.h>
#include <limits.h>

#include "die.h"
#include "xfuncs.h"

void *xmalloc(size_t size)
{
	void *mem = malloc(size);
	if (!mem) die_oom();
	return mem;
}


long xatol(const char *s)
{
	char *end;
	long res = strtol(s, &end, 10);
	if (*end) {
		fprintf(stderr, "%s: not a number: %s\n", __func__, s);
		exit(1);
	}
	return res;
}



void xgetaddrinfo(const char *node, const char *service,
		struct addrinfo *hints, struct addrinfo **res)
{
	int ret;

	ret = getaddrinfo(node, service, hints, res);
	if (ret != 0) {
	       if (ret == EAI_SYSTEM)
		       die_sys(111, "getaddrinfo() failed");
		fprintf(stderr, "getaddrinfo() failed: %s\n", gai_strerror(ret));
		exit(1);
	}
}

