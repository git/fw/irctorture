/*
 * simple IRC protocol abstraction.
 * "simple", because there is no parsing of sent/received data
 * beyond checks for complete messages (i.e. line terminators, \r\n).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License
 * as published by the Free Software Foundation.
 */
#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>

#include "array.h"
#include "irc.h"

#define MAX_IRC_CMD_LEN		512
#define DEFAULT_RBUF_SIZE	4096
#define DEFAULT_WBUF_SIZE	1024

/* internal states of an irclayer connection */
enum irclayer_conn_state {
	IRC_CONN_CLOSED,	/* underlying pull function returned EOF */
	IRC_CONN_ERROR,		/* underlying pull function returned fatal error */
	IRC_CONN_NEEDLOGIN,	/* initial state after _register(), user must call irclayer_login() */
	IRC_CONN_LOGIN_SENT,	/* irclayer_login() has sent NICK/USER command, must wait for server welcome msg */
	IRC_CONN_ACTIVE		/* irclayer_login suceeded, irc connection registered with irc server */
};

/*
 * optional statistics of bytes read/written
 * and the way writes were performed, i.e.
 * the amount of additional copying/buffering done.
 *
 * This is a per-connection structure. Statistics
 * for all connection is accumulated as well.
 */
#ifdef IRC_WITH_STATS
struct ircstat {
	unsigned long written, read;
	unsigned int writes_direct, writes_buffered;
};
static struct ircstat stat_total;
#endif


struct irc_connection {
	int flags;
	array rbuf, wbuf;
	enum irclayer_conn_state state;
	int conn_errno;
#ifdef IRC_WITH_STATS
	struct ircstat stat;
#endif
	union {
		irclayer_transport_ptr_t *ptr;
		int fd;
	} pushsock, pullsock;
	ssize_t (*irclayer_push_func)(irclayer_transport_ptr_t, const void*, size_t);
	ssize_t (*irclayer_pull_func)(irclayer_transport_ptr_t, void*, size_t);
	void *cookie;
};


static array connections;

static ssize_t do_irclayer_read(struct irc_connection *c, void *buf, size_t buflen);


static struct irc_connection *conn_get(int fd)
{
	struct irc_connection *conn;
	assert(fd >= 0);

	conn = array_get(&connections, sizeof(*conn), fd);
	return conn;
}

/*
 * bookkeeping functions for read/write statistic tracking,
 * defaults to off.
 */
#ifdef IRC_WITH_STATS
static void ircstats_init(struct irc_connection *c)
{
	c->stat.written = 0;
	c->stat.read = 0;
	c->stat.writes_direct = 0;
	c->stat.writes_buffered = 0;
}


void ircstats_print(struct irc_connection *c)
{
	struct ircstat *statptr;

	if (c)
		statptr = &c->stat;
	else
		statptr = &stat_total;
	printf("stat.written = %lu, stat.read = %lu, "
		"stat.writes_direct = %u, "
		"stat.writes_buffered = %u\n", statptr->written,
		statptr->read, statptr->writes_direct,  statptr->writes_buffered);
}


static void ircstats_acct_direct_write(struct irc_connection *c)
{
	c->stat.writes_direct++;
	stat_total.writes_direct++;
}

static void ircstats_acct_buffered_write(struct irc_connection *c)
{
	c->stat.writes_buffered++;
	stat_total.writes_buffered++;
}

static void ircstats_acct_written(struct irc_connection *c, ssize_t written)
{
	if (written > 0) {
		c->stat.written += written;
		stat_total.written += written;
	}
}


static void ircstats_acct_read(struct irc_connection *c, ssize_t read)
{
	if (read > 0) {
		c->stat.read += read;
		stat_total.read += read;
	}
}
#else
static inline void ircstats_init(struct irc_connection *c) { (void) c; }
static inline void ircstats_acct_direct_write(struct irc_connection *c) { (void) c; }
static inline void ircstats_acct_buffered_write(struct irc_connection *c) { (void) c; }
static inline void ircstats_acct_written(struct irc_connection *c, ssize_t written) { (void)c; (void) written; }
static inline void ircstats_acct_read(struct irc_connection *c, ssize_t read) { (void)c; (void) read; }
#endif


/* return number of characters parsed (ie amount of digits) */
static size_t scan_int(const char *str, int *val)
{
        char *endptr;
        long num;
        size_t parsed;

        num = strtol(str, &endptr, 10);
        parsed = endptr - str;
	if (parsed)
		*val = (int) num;
	return parsed;
}


static int str_to_statuscode(const char *line)
{
	/* :irc.example.org 433 * nick :Nickname already in use */
	int code;
	const char *code_str = strchr(line, ' ');
	if (code_str && scan_int(code_str, &code))
		return code;
	return -1;
}


static ssize_t do_read(struct irc_connection *c, void *buf, size_t buflen)
{
	ssize_t br;
	assert(c);
	br = c->irclayer_pull_func(c->pullsock.ptr, buf, buflen);
	if (br == IRCERR_SYSTEM && (errno == EAGAIN || errno == EINTR))
		return IRCERR_WANTREAD;
	ircstats_acct_read(c, br);
	return br;
}


/*
 * 0: nickname not in use
 * < 0: error (IRCERR_NICKNAMEINUSE, IRCERR_SYSTEM, _WANTREAD, etc.
 */
static int login_nickname_inuse(struct irc_connection *c)
{
	int ret = IRCERR_WANTREAD;

	for (;;) {
		int code;
		char buf[MAX_IRC_CMD_LEN + 1];
		ssize_t br = do_irclayer_read(c, buf, sizeof(buf) -1);

		if (br < 0)
			return br;
		if (br == 0) /* EOF, can't return 0 */
			return IRCERR_NOTCONN;
		buf[br] = 0;
		/*
		 * complete (or overly long... ) irc line, check
		 * if it contains an irc status code number
		 */
		code = str_to_statuscode(buf);
		if (code > 400) { /* error */
			ret = IRCERR_NICKNAMEINUSE;
			break;
		}

		if (code == 20) /* RPL_HELLO, sent by irc 2.11 on connect */
			break;
		if (code > 0) /* okay */
			return 0;
	}
	return ret;
}


static int wait_for_server_welcome_msg(struct irc_connection *c)
{
	int ret = login_nickname_inuse(c);
	if (ret == 0)
		c->state = IRC_CONN_ACTIVE;
	return ret;
}


static int do_login(int fd, const char *nick, const char *ident, const char *realname)
{
	char buf[MAX_IRC_CMD_LEN + 1];
	ssize_t written;
	int ret;
	struct irc_connection *c;

	snprintf(buf, sizeof(buf), "NICK %s\r\n", nick);
	written = irclayer_write(fd, buf, strlen(buf));
	if (written < 0)
		return written;

	c = conn_get(fd);
	assert(c);

	/*
	 * done before registration complete, so will
	 * never return 0 at this point (IRC server does only send a NICK message
	 * for registered connections; and we aren't registered yet...)
	 */
	ret = login_nickname_inuse(c);
	assert(ret != 0);
	switch (ret) {
	case IRCERR_WANTREAD: /* proceed to USER command */
		break;
	default: return ret; /* nick collision; or error */
	}

	snprintf(buf, sizeof(buf), "USER %s %s - %s\r\n", ident, nick, realname);
	written = irclayer_write(fd, buf, strlen(buf));
	if (written > 0) {
		c = conn_get(fd);
		assert(c);
		c->state = IRC_CONN_LOGIN_SENT;
		return wait_for_server_welcome_msg(c);
	}
	return IRCERR_SYSTEM;
}


static struct irc_connection *irc_connection_new(int fd)
{
	struct irc_connection *c;

	assert(fd >= 0);

	if (fd < 0) {
		errno = -EBADF;
		return NULL;
	}
	c = array_alloc(&connections, sizeof(*c), fd);
	if (c) {
		c->state = IRC_CONN_CLOSED;
		array_init(&c->wbuf);
		array_init(&c->rbuf);
		c->cookie = NULL;
		ircstats_init(c);
	}
	return c;
}


void irclayer_transport_set_pull_function(int fd, irclayer_pull_func func)
{
	struct irc_connection *c = conn_get(fd);
	if (c)
		c->irclayer_pull_func = func;
}


void irclayer_transport_set_push_function(int fd, irclayer_push_func func)
{
	struct irc_connection *c = conn_get(fd);
	if (c)
		c->irclayer_push_func = func;
}


void irclayer_transport_set_ptr(int fd, irclayer_transport_ptr_t ptr)
{
	struct irc_connection *c = conn_get(fd);
	if (c) {
		c->pushsock.ptr = ptr;
		c->pullsock.ptr = ptr;
	}
}


/*
 * must be called before any other irclayer_* function.
 *
 * cannot be merged into irclayer_login(), because irclayer_login
 * sends data, and we must allow a user to change the lowlevel read/write
 * routines (irclayer_transport_set_push_function, irclayer_transport_set_ptr etc.),
 * which cannot be done before irclayer_register().
 */
bool irclayer_register(int fd)
{
	struct irc_connection *c = conn_get(fd);
	if (c == NULL) {
		c = irc_connection_new(fd);
		if (c == NULL)
			return false;
	} else {
		array_trunc(&c->rbuf);
		array_trunc(&c->wbuf);
	}
	c->state = IRC_CONN_NEEDLOGIN;
	c->cookie = NULL;
	c->pushsock.fd = fd;
	c->pullsock.fd = fd;
	c->irclayer_push_func = (irclayer_push_func) write;
	c->irclayer_pull_func = (irclayer_pull_func) read;
	return true;
}


int irclayer_login(int fd, const char *nick, const char *ident, const char *realname)
{
	struct irc_connection *c = conn_get(fd);

	if (!c)
		return IRCERR_BADF;

	switch (c->state) {
	case IRC_CONN_ACTIVE: return 0;
	case IRC_CONN_CLOSED: return IRCERR_NOTCONN;
	case IRC_CONN_LOGIN_SENT:
		return wait_for_server_welcome_msg(c);
	case IRC_CONN_NEEDLOGIN:
		return do_login(fd, nick, ident, realname);
	case IRC_CONN_ERROR:
		errno = c->conn_errno;
		return IRCERR_SYSTEM;
	}
	return IRCERR_NOTCONN;
}


static int status_to_errcode(struct irc_connection *c)
{
	enum irclayer_conn_state state = c->state;

	switch (state) {
	case IRC_CONN_ACTIVE: return IRCERR_WANTREAD;
	case IRC_CONN_LOGIN_SENT:
	case IRC_CONN_NEEDLOGIN:
		return IRCERR_NOTCONN;
	case IRC_CONN_CLOSED: return 0;
	case IRC_CONN_ERROR: errno = c->conn_errno; return IRCERR_SYSTEM;
	}
	return IRCERR_NOTCONN;
}


/*
 * remove data from the internal buffer and put it into
 * the user buffer, buf.
 *
 * This is usually called in one of three cases:
 * a) error on socket, and internal buffer isn't empty yet.
 * b) it was found that internal buffer contains at least one complete
 *    irc command. In this case, len is the length of that command and
 *    the command is extracted from the internal buffer.
 * c) the internal buffer contains data and the user called irclayer_read
 *    with a buffer size smaller than the number of bytes in the internal
 *    buffer, i.e. we put an incomplete/partial line into *buf.
 *    NOTE: c) will never happen if the IRC server is RFC compliant AND
 *    the buffer passed in by the user can hold at least 512 bytes.
 */
static ssize_t rbuf_drain(struct irc_connection *c, void *buf, size_t len)
{
	void *rbufptr;
	size_t rbuf_len;

	assert(c);
	assert(buf);
	assert(len);

	rbufptr = array_start(&c->rbuf);
	rbuf_len = array_bytes(&c->rbuf);

	/*
	 * do not copy more than the number of
	 * available bytes...
	 */
	if (rbuf_len < len)
		len = rbuf_len;

	if (len) {
		memcpy(buf, rbufptr, len);
		array_moveleft(&c->rbuf, 1, len);
	}
	return len;
}


static inline ssize_t do_write(struct irc_connection *c, const void *buf, size_t buflen)
{
	ssize_t written;

	assert(c);

	if (buflen == 0)
		return 0;
	written = c->irclayer_push_func(c->pushsock.ptr, buf, buflen);
	if (written == IRCERR_SYSTEM && (errno == EAGAIN || errno == EINTR))
		return IRCERR_WANTWRITE;

	ircstats_acct_written(c, written);
	return written;
}


/*
 * Called when the internal buffer contains data without the end-of-line marker, \r\n.
 *
 * This function continues reading until:
 * a) \r\n is seen,
 * b) the underlying * read function returns EOF or an error,
 * c) the buffer passed by the user is not large enough to hold all data read so far, or
 * d) we read more than DEFAULT_RBUF_SIZE bytes without seeing an end-of-line
 *    marker (if this happens, the remote irc server is not RFC compliant).
 */
static ssize_t read_continue_partial(struct irc_connection *c, void *buf, size_t buflen)
{
	size_t len;
	bool gotline = false;

	assert(buflen > 0);

	while (!gotline) {
		char rbuf[MAX_IRC_CMD_LEN+1];
		char *rbufptr;
		ssize_t br;

		len = array_bytes(&c->rbuf);
		if (len >= DEFAULT_RBUF_SIZE || buflen <= len)
			return rbuf_drain(c, buf, buflen);

		/*
		 * read() is called until we get an error (including EAGAIN),
		 * or a complete line was read (i.e. rbufptr[len] == \r or \n).
		 */
		br = do_read(c, rbuf, sizeof(rbuf));
		if (br < 0) {
			if (br != IRCERR_SYSTEM)
				return br;
			c->conn_errno = errno;
			c->state = IRC_CONN_ERROR;
			return rbuf_drain(c, buf, buflen);
		}
		if (br == 0) {
			c->state = IRC_CONN_CLOSED;
			return rbuf_drain(c, buf, buflen);
		}
		if (!array_catb(&c->rbuf, rbuf, br))
			return IRCERR_SYSTEM;

		if (!array_cat0_temporary(&c->rbuf))
			return IRCERR_SYSTEM;

		rbufptr = array_start(&c->rbuf);
		len = strcspn(rbufptr, "\r\n");
		assert(len <= array_bytes(&c->rbuf));
		switch (rbufptr[len]) {
		case '\r':
			   len++;
			   if (rbufptr[len] != '\n')
				   break;
			   /* fallthrough */
		case '\n':
			   len++;
			   gotline = true;
		}
	}
	/* at least one complete command in rbuf */
	if (buflen < len)
		len = buflen;
	return rbuf_drain(c, buf, len);
}


/*
 * Called from do_irclayer_read() when the internal buffer is
 * empty. In this case, we try to get around extra copying
 * from/to the internal buffer by reading into the user buffer, buf.
 *
 * If more then one irc command is read, the remaining data (after \r\n)
 * is moved to the internal buffer.
 *
 * If an incomplete command is read, all data is moved to the internal
 * buffer.
 */
static ssize_t read_full_line(struct irc_connection *c, char *buf, size_t buflen)
{
	ssize_t br;
	size_t len, line_len;

	assert(buflen > 0);

	/* no point continuing if we can't even store \r\n ...*/
	if (buflen < 2)
		return do_read(c, buf, buflen);

	len = buflen - 1;

	assert(array_bytes(&c->rbuf) == 0);
	br = do_read(c, buf, len);
	if (br < 0) {
		if (br != IRCERR_SYSTEM)
			return br;
		c->conn_errno = errno;
		c->state = IRC_CONN_ERROR;
		return rbuf_drain(c, buf, buflen);
	}
	if (br == 0) {
		c->state = IRC_CONN_CLOSED;
		return rbuf_drain(c, buf, buflen);
	}
	buf[br] = 0;

	line_len = strcspn(buf, "\r\n");
	switch (buf[line_len]) {
	case '\r':
		if (buf[line_len+1] == '\n') {
			line_len +=2;
			break;
		}
		/* fallthrough */
	case 0: /* not \r\n or \n -> not a complete line */
		if (!array_copyb(&c->rbuf, buf, br))
			return IRCERR_SYSTEM;
		/* incomplete line, try again. */
		return read_continue_partial(c, buf, buflen);
	case '\n':
		line_len++;
		break;
	}

	assert(line_len <= (size_t)br);
	br -= line_len;
	/* if we received more than a single command, store rest in rbuf */
	if (br && !array_copyb(&c->rbuf, buf + line_len, br))
		return IRCERR_SYSTEM;

	if (line_len == 0)
		return IRCERR_WANTREAD;
	return line_len;
}


static ssize_t do_irclayer_read(struct irc_connection *c, void *buf, size_t buflen)
{
	assert(c);

	for (;;) {
		size_t linelen;
		char *rbufptr;

		if (array_bytes(&c->rbuf) == 0)
			return read_full_line(c, buf, buflen);

		if (!array_cat0_temporary(&c->rbuf))
			return IRCERR_SYSTEM;
		rbufptr = array_start(&c->rbuf);

		/* check if we have a _complete_ irc command */
		linelen = strcspn(rbufptr, "\r\n");
		if (linelen) { /* okay, we have a few characters other than \r\n ... */
			assert(linelen <= array_bytes(&c->rbuf));
			assert(linelen < c->rbuf.allocated);
			if (rbufptr[linelen]) {
				/*
				 * ... and we found \r\n before hitting \0,
				 * so its a complete command
				 */
				size_t to_copy = buflen > linelen ? linelen : buflen;
				return rbuf_drain(c, buf, to_copy);
			}
			/* incomplete command. */
			if (buflen <= linelen) /* not enough space; don't bother reading more data */
				return rbuf_drain(c, buf, buflen);

			/* incomplete command and enough space; try to read more. */
			break;
		}
		/* no characters other than \r\n, discard them and check again */
		linelen = strspn(rbufptr, " \t\r\n");
		if (!linelen) /* zero-bytes from remote end?! */
			linelen = 1; /* skip */
		array_moveleft(&c->rbuf, 1, linelen);
	}

	return read_continue_partial(c, buf, buflen);
}


/*
 * This just calls do_irclayer_read after checking
 * that fd is known and the connection active.
 */
ssize_t irclayer_read(int fd, void *buf, size_t buflen)
{
	struct irc_connection *c = conn_get(fd);

	if (!c)
		return IRCERR_BADF;

	if (c->state != IRC_CONN_ACTIVE)
		return status_to_errcode(c);

	return do_irclayer_read(c, buf, buflen);
}


static ssize_t wbuf_flush(struct irc_connection *c)
{
	size_t len = array_bytes(&c->wbuf);
	const void *buf = array_start(&c->wbuf);
	ssize_t bw;

	if (!len)
		return 0;

	assert(buf);

	bw = do_write(c, buf, len);
	if (bw < 0)
		return bw;

	ircstats_acct_buffered_write(c);

	if (bw == (ssize_t)len) {
		array_trunc(&c->wbuf);
		return bw;
	}
	array_moveleft(&c->wbuf, 1, bw);
	return bw;
}


static ssize_t do_irclayer_write(struct irc_connection *c, const void *buffer, size_t buflen)
{
	ssize_t bw;
	size_t len;
	const char *buf = buffer;

	len = array_bytes(&c->wbuf);
	if (len > 0) {
		bw = wbuf_flush(c);
		if (bw < 0) /* couldn't flush buffer */
			return bw;
		len = array_bytes(&c->wbuf);
		if (len > DEFAULT_WBUF_SIZE) {
			/* refuse to queue up more than this... */
			errno = ENOMEM;
			return IRCERR_SYSTEM;
		}
		if (len > 0) {
			/* queue data for later... */
			if (buflen && !array_catb(&c->wbuf, buf, buflen))
				return IRCERR_SYSTEM;
			return IRCERR_WANTWRITE;
		}
		if (buflen == 0) {
			/* no data to send */
			return bw;
		}
	}
	/* wbuf is empty, and there is data to send. */
	bw = do_write(c, buf, buflen);
	if (bw < 0) {
		if (bw == IRCERR_WANTWRITE && !array_copyb(&c->wbuf, buf, buflen))
			return IRCERR_SYSTEM;
		return bw;
	}
	ircstats_acct_direct_write(c);
	if ((size_t)bw < buflen) {
		/* partial write, queue up the rest */
		buflen -= bw;
		if (!array_copyb(&c->wbuf, buf + bw, buflen))
			return IRCERR_SYSTEM;
		return IRCERR_WANTWRITE;
	}
	return bw;
}


ssize_t irclayer_write(int fd, const void *buffer, size_t buflen)
{
	struct irc_connection *c = conn_get(fd);
	assert(c);
	if (!c)
		return IRCERR_BADF;
	return do_irclayer_write(c, buffer, buflen);
}


static void irclayer_free(struct irc_connection *c)
{
	c->state = IRC_CONN_CLOSED;
	array_free(&c->rbuf);
	array_free(&c->wbuf);
}


bool irclayer_quit(int fd)
{
	bool ret = false;
	struct irc_connection *conn = conn_get(fd);
	static const char quit[6] = "QUIT\r\n";/* not NUL terminated */

	if (conn) {
		if (conn->state == IRC_CONN_ACTIVE &&
			do_irclayer_write(conn, quit, sizeof(quit)) > 0 &&
			(array_bytes(&conn->wbuf) == 0))
		{
			ret = true;
		}
		irclayer_free(conn);
	}
	return ret;
}


const char *irclayer_strerror(int err)
{
	enum irclayer_errno x = err;

	switch (x) {
	case IRCERR_SYSTEM: return "system error";
	case IRCERR_NICKNAMEINUSE: return "Nick name alread exists";
	case IRCERR_NOTCONN: return "Not connected";
	case IRCERR_WANTWRITE: return "Need write";
	case IRCERR_WANTREAD: return "Need read";
	case IRCERR_BADF: return "Bad file descriptor";
	}
	return "Unknown error";
}


ssize_t irclayer_printf(int fd, const char *format, ...)
{
	char msg[MAX_IRC_CMD_LEN + 1];
	va_list ap;

	assert(format != NULL);

	va_start(ap, format);
	vsnprintf(msg, sizeof(msg), format, ap);
	va_end(ap);

	return irclayer_write(fd, msg, strlen(msg));
}


void *irclayer_getcookie(int fd)
{
	struct irc_connection *c = conn_get(fd);
	assert(c);
	if (!c)
		return NULL;
	return c->cookie;
}


bool irclayer_setcookie(int fd, void *cookie)
{
	struct irc_connection *c = conn_get(fd);
	assert(c);
	if (!c)
		return false;
	c->cookie = cookie;
	return true;
}

