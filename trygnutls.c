#include <gnutls/gnutls.h>

int main(void)
{
	char buf;

	gnutls_session_t tls;
	gnutls_transport_set_ptr(tls, 0);
	return gnutls_record_recv(tls, &buf, 1);
}
