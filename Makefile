prefix=/tmp
SBINDIR=${prefix}/sbin
MAN1DIR=${prefix}/share/man/man1

all: irctorture
CC=gcc
CFLAGS=-std=gnu99 -W -Wall -pedantic -Wformat-nonliteral -Wformat-security
#CFLAGS+=-fstack-protector -g
OBJ=array.o async_connect.o die.o fuzzer.o portab.o tls.o io.o irc.o xfuncs.o
IO_TEST_HDRS=haveepoll.h havekqueue.h


tls.o: tls.c tls.h array.h irc.h havegnutls.h
	$(CC) $(CFLAGS) $(DEFS) -c tls.c

io.o: io.c io.h array.h $(IO_TEST_HDRS) defines.h conn.h resolve.h
	$(CC) $(CFLAGS) $(DEFS) -c io.c

%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c $^

main.o: main.c $(IO_TEST_HDRS) havegnutls.h irctorture.h
	$(CC) $(CFLAGS) $(DEFS) -c main.c

irctorture: main.o $(OBJ)
	$(CC) $(CFLAGS) $(DEFS) main.o `pkg-config --libs gnutls` $(OBJ) -o irctorture

doc: irctorture.1

irctorture.1: man/irctorture.pod
	pod2man -r " " -c " " --section=1 $< $@

clean:
	rm -f $(IO_TEST_HDRS)
	rm -f havegnutls.h
	rm -f defines.h conn.h resolve.h
	rm -f defines.h.gch conn.h.gch resolve.h.gch
	rm -f irctorture
	rm -f *.o

tags:
	ctags -R *.c *.h

install: strip
	test -d ${BINDIR} || mkdir -p  ${BINDIR}
	install -m 755 irctorture ${BINDIR}/irctorture
	test -d ${MAN1DIR} || mkdir -p ${MAN1DIR}
	install -m 644 irctorture.1 ${MAN1DIR}

strip:
	strip -R .note -R .comment irctorture

uninstall:
	rm -f ${SBINDIR}/irctorture
	rm -f ${MAN8DIR}/irctorture.1
	rm -f ${MAN8DIR}/irctorture.1.gz

haveepoll.h: tryepoll.c
	@echo "/* for io.h */" > $@
	if $(CC) $(CFLAGS) -o /dev/null tryepoll.c >/dev/null 2>&1; then echo "#define HAVE_EPOLL_CREATE 1";fi >> $@

havekqueue.h: trykqueue.c
	@echo "/* for io.h */" > $@
	if $(CC) $(CFLAGS) -o /dev/null trykqueue.c >/dev/null 2>&1; then echo "#define HAVE_KQUEUE 1";fi >> $@


havegnutls.h: trygnutls.c
	@echo "/* for gnutls.h */" > $@
	if $(CC) $(CFLAGS) -c -o /dev/null trygnutls.c >/dev/null 2>&1; then echo "#define HAVE_GNUTLS 1";fi >> $@


# needed by io.c
# create empty files so we can use
# unmodified io and array from ngircd
defines.h:
	@echo "/* for io.h */" > $@

resolve.h:
	@echo "/* for io.h */" > $@

conn.h:
	@echo "/* for io.h */" > $@

.PHONY: all clean doc
