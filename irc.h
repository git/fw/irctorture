#include <stdbool.h>
#include <stddef.h>
#include <unistd.h>

enum irclayer_errno {
	IRCERR_BADF = -128,	/* file descriptor isn't registered at the irc layer */
	IRCERR_WANTWRITE = -32,	/* network congestion; need to wait until socket becomes writeable again */
	IRCERR_WANTREAD = -16,	/* network congestion; need to wait until socket becomes readable again */
	IRCERR_NOTCONN = -4,	/* disconnected */
	IRCERR_NICKNAMEINUSE = -2, /* could not log in: nick name was already registered by someone else */
	IRCERR_SYSTEM = -1	/* syscall failed, check errno */
};


/*
 * first argument for custom send/write functions.
 * It is up to the transport function to interpret this.
 */
typedef void *irclayer_transport_ptr_t;

/*
 * read/write function prototypes.
 * MUST return -1 for errors, IRCERR_WANTWRITE (or _WANTREAD), 0 for EOF, or
 * the number of bytes written.
 */
typedef ssize_t (*irclayer_pull_func)(irclayer_transport_ptr_t, void *, size_t);
typedef ssize_t (*irclayer_push_func)(irclayer_transport_ptr_t, const void *, size_t);


/*
 * set functions to send/write the data.
 * Both functions are optional, by default plain read(2) and write(2) are used.
 * The prototypes are the same as for read(2) and write(2), with the exception
 * that the first argument must be of type 'irclayer_transport_ptr_t', see above.
 */
void irclayer_transport_set_pull_function(int fd, irclayer_pull_func func);
void irclayer_transport_set_push_function(int fd, irclayer_push_func func);

/*
 * If you changed the default pull/push function, you may also
 * want to change the first argument of the transport functions.
 * default is fd itself.
 */
void irclayer_transport_set_ptr(int fd, irclayer_transport_ptr_t ptr);

/*
 * register fd with irc layer (fd should be an established connection)
 * Must be called on an fd before any other irclayer_ functions.
 *
 * true: fd registered
 * false: error (e.g. out of memory)
 */
bool irclayer_register(int fd);

/*
 * try to login to irc server using given nick/ident/realname.
 * returns 0 on success or one of the irclayer_errno conditions.
 */
int irclayer_login(int fd, const char *nick, const char *ident, const char *realname);


/*
 * write data to irc layer. returns amount of data written or one of
 * the irclayer_errno conditions.
 *
 * Data that cannot be written out immediately will be queued, and
 * IRCERR_WANTWRITE is returned.
 * You can call this function with a 0 length buffer to flush
 * the internal buffer.
 *
 * You may also use select(2) or similar to test when the socket
 * becomes writeable again.
 */
ssize_t irclayer_write(int fd, const void *buf, size_t buflen);

/* irclayer_write wrapper that accepts a printf(3)-Style format string */
ssize_t irclayer_printf(int fd, const char *format, ...);

/*
 * read data from irc layer.
 * If buffer is large enough a single, unaltered irc command (i.e. including \r\n)
 * is returned.  Result is NOT null terminated.
 * returns number of bytes read, 0 on EOF or one of the irclayer_errno conditions.
 * Note: uses internal buffering if necessary; you should call this function
 * repeatedly until it returns an error.
 */
ssize_t irclayer_read(int fd, void *buf, size_t buflen);

/*
 * send QUIT\r\n to peer and unregisters fd
 * from irc layer. The file descriptor is NOT closed.
 */
bool irclayer_quit(int fd);

/*
 * return a string constant describing the irclayer_errno.
 * This function never returns NULL, even when err
 * is not an irclayer_errno value.
 */
const char *irclayer_strerror(int err);

/* attach cookie to fd; cookie is transparent to irclayer_ functions. */
bool irclayer_setcookie(int fd, void *cookie);
void *irclayer_getcookie(int fd);

