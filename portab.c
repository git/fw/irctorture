#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

#include "log.h"
#include "portab.h"

void Log(int UNUSED Level, const char *Format, ... )
{
        char msg[1024];
        va_list ap;

        assert( Format != NULL );

        va_start(ap, Format);

        vsnprintf(msg, sizeof(msg), Format, ap);
        va_end(ap);

	fprintf(stdout, "%s\n", msg);
}
