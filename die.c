/*
 * This file is in the public domain.
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "die.h"

void die(int e, const char *s)
{
	fputs(s, stderr);
	fputc('\n', stderr);
	exit(e);
}


void die_sys(int e, const char *s)
{
	fprintf(stderr, "%s: %s\n", s, strerror(errno));
	exit(e);
}

void die_oom(void)
{
	die(111, "Out of memory");
}
