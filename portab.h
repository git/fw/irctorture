#include <stdbool.h>
#include <stddef.h>

#define ARRAY_SIZE(x)	(sizeof(x) / sizeof((x)[0]))
#define PARAMS( args )	args
#define PROTOTYPES

#ifdef __GNUC__
#define UNUSED __attribute__((__unused__))
#else
#define UNUSED
#endif

#define __stringify_1(x)        #x
#define __stringify(x)          __stringify_1(x)

#define HAVE_SELECT
#include "haveepoll.h"
#include "havekqueue.h"

