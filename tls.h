#include <stdbool.h>

enum tls_error {
	TLSERR_WANTWRITE = -4, /* TLS layer needs to write data, check for writeablility and try again */
	TLSERR_WANTREAD = -2, /* TLS layer must read data first, check for readability and try again */
	TLSERR_SYSTEM = -1 /* syscall error, check errno */
};

/* must be called at startup to init tls library */
void tls_init(void);

/*
 * starts TLS handshake. The underlying transport protocol must
 * be in a connected state. Can be used with nonblocking sockets.
 * In this case, this function may return TLSERR_WANTWRITE or
 * TLSERR_WANTREAD. Call the function again after select/poll, etc.
 * indicate state change on the file descriptor.
 * returns 0 on success.
 * The irc_write, irc_read etc. functions will
 * use TLS read/write functions internally. */
int tls_connect(int fd);

/* closes tls session and removes fd from tls layer */
void tls_shutdown(int fd);
