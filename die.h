/*
 * terminate program in case of non-recoverable errors
 */
void die_oom(void);

/* terminate program after writing the string pointed to by msg to stderr */
void die(int e, const char *msg);
/* terminate program after writing errno and string pointed to by msg to stderr */
void die_sys(int retv, const char *msg);



