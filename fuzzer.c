/*
<message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf>
<prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ]
<command>  ::= <letter> { <letter> } | <number> <number> <number>
<SPACE>    ::= ' ' { ' ' }
<params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ]
<middle>   ::= <Any *non-empty* sequence of octets not including SPACE
               or NUL or CR or LF, the first of which may not be ':'>
<trailing> ::= <Any, possibly *empty*, sequence of octets not including
                 NUL or CR or LF>
<crlf>     ::= CR LF

<target>     ::= <to> [ "," <target> ]
<to>         ::= <channel> | <user> '@' <servername> | <nick> | <mask>
<channel>    ::= ('#' | '&') <chstring>
<servername> ::= <host>
<host>       ::= see RFC 952 [DNS:4] for details on allowed hostnames
<nick>       ::= <letter> { <letter> | <number> | <special> }
<mask>       ::= ('#' | '$') <chstring>
<chstring>   ::= <any 8bit code except SPACE, BELL, NUL, CR, LF and comma (',')>
*/

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <netinet/in.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

#include "array.h"
#include "fuzzer.h"
#include "irctorture.h"
#include "portab.h"


#define MAX_RECURSION_DEPTH 50

enum command_id {
	ADMIN, AWAY, CHANINFO, CONNECT, DIE, DISCONNECT,
	ERROR, HELP, INVITE, ISON, JOIN, KICK, KILL,
	LINKS, LIST, LUSERS, MODE, MOTD, NAMES, NICK,
	NJOIN, NOTICE,  OPER, OPERWALL, PART, PASS,
	PING,PONG, PRIVMSG, QUIT, REHASH, RESTART,
	SERVER, SQUIT, STATS, TIME, TOPIC, TRACE,
	USER, USERHOST, VERSION, WHO, WHOIS, WHOWAS,
	COMMAND_ID_MAX
};

enum symbols {
	CHANNEL = COMMAND_ID_MAX,
	CHR_AND, CHR_AT, CHR_BANG, CHR_COLON, CHR_DOLLAR,
	CHR_HASH, CHR_PLUSMINUS, CHR_SEPERATOR, CHR_SPACE,
	CHR_MODE, CHR_NUMBER,

	CHSTRING,
	COMMAND,
	CRLF,	/* add \r\n to output buffer & stop looking for more symbols */
	HOST,
	MASK,
	MESSAGE,
	MIDDLE,
	SYM_NICK,
	PARAMS,
	PREFIX,
	SERVERNAME,
	TARGET,
	TO,
	TRAILING,
	SYM_USER,

	EOR	/* end of rule chain; like CRLF but only stop, do not add \r\n to output buffer */
};

/* <message>  ::= [':' <prefix> <SPACE> ] <command> <params> <crlf> */
static int rules_message[][7] = {
	{ CHR_SEPERATOR, PREFIX, CHR_SPACE, COMMAND, PARAMS, CRLF, EOR },
	{ COMMAND, PARAMS, CRLF, EOR }
};


/* <prefix>   ::= <servername> | <nick> [ '!' <user> ] [ '@' <host> ] */
static int rules_prefix[][6] = {
	{ SERVERNAME, EOR },
	{ SYM_NICK, EOR },
	{ SYM_NICK, CHR_BANG, SYM_USER, EOR },
	{ SYM_NICK, CHR_AT, HOST, EOR },
	{ SYM_NICK, CHR_BANG, SYM_USER, CHR_AT, HOST, EOR }
};

/* <command>  ::= <letter> { <letter> } | <number> <number> <number> */
/* handled specially: see get_command() below */


/*
 * 'normal' structure of commands and their arguments.
 *  The main generator code first picks a random command
 *  name off this list, then a
 *  ruleset within.
 *  rows is the total number of rulesets available for a certain command.
 */
#define X(val) __stringify(val)
static const struct {
	const char *name;
	int ruleset[9][12];
	unsigned rows;
} commands[] = {
	{ X(ADMIN), {{EOR}}, 0},
	{ X(AWAY), {{EOR}}, 0 },
	{ X(CHANINFO), {{EOR}}, 0 },
	{ X(CONNECT),  {{EOR}}, 0 },
	{ X(DIE), {{EOR}},0 },
	{ X(DISCONNECT), {{EOR}},0 },
	{ X(ERROR),  {{EOR }}, 0},
	{ X(HELP), {{EOR}}, 0 },
	{ X(INVITE), {
		{ SYM_NICK, CHR_SPACE, CHANNEL, CRLF },
		{ EOR }
	}, 2},
	{ X(ISON), {{EOR}}, 0},
	{ X(JOIN), {
		{ CHANNEL, CRLF, EOR },
		{ CHANNEL, CHR_SPACE, CHSTRING, CRLF },
		{ CHANNEL, CHR_SEPERATOR, CHANNEL, CRLF },
		{ CHANNEL, CHR_SEPERATOR, CHANNEL, CHR_SPACE, CHSTRING,CRLF },
		{ CHANNEL, CHR_SEPERATOR, CHANNEL, CHR_SPACE, CHSTRING, CHR_SEPERATOR, CHSTRING, CRLF },
		{ EOR }
	}, 6 },
	{ X(KICK), {
		{ CHANNEL, CHR_SPACE, SYM_USER, CHR_SPACE, CHSTRING, CRLF },
		{ CHANNEL, CHR_SPACE, SYM_USER, CRLF },
		{ EOR }
	}, 3},
	{ X(KILL), {{EOR}}, 0},
	{ X(LINKS), {{EOR}}, 0},
	{ X(LIST), {
		{ CHANNEL, CRLF },
		{ CHANNEL, CHR_SPACE, SERVERNAME, CRLF },
		{ CHANNEL, CHR_SEPERATOR, CHANNEL, CHR_SPACE, SERVERNAME, CRLF },
		{ CHANNEL, CHR_SEPERATOR, CHANNEL, CRLF },
		{ EOR }
	}, 5 },
	{ X(LUSERS), {{EOR}},0},
	{ X(MODE), {
		/* channel modes */
		{ CHANNEL, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, CRLF },
		{ CHANNEL, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, CHR_NUMBER, CHR_SPACE, SYM_USER, CHR_SPACE, MASK, CRLF },
		{ CHANNEL, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, CHR_NUMBER, CHR_SPACE, SYM_USER, CRLF},
		{ CHANNEL, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, CHR_NUMBER, CRLF },
		{ CHANNEL, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, CHR_NUMBER, CHR_SPACE, MASK, CRLF },
		{ CHANNEL, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, CHR_NUMBER, CRLF },
		{ CHANNEL, CHR_SPACE,  CHR_PLUSMINUS, CHR_MODE, CHR_SPACE, SYM_USER, CRLF },
		/* user modes */
		{ SYM_NICK, CHR_SPACE, CHR_PLUSMINUS, CHR_MODE, CRLF },
		{ EOR }
	}, 9 },
	{ X(MOTD), {{EOR}}, 0},
	{ X(NAMES), {
		{ CHANNEL, CHR_SEPERATOR, CHANNEL, CRLF },
		{ CHANNEL, CRLF },
		{ EOR }
	}, 3},
	{ X(NICK), {{EOR}}, 0},
	{ X(NJOIN), {{EOR}}, 0},
	{ X(NOTICE), {{EOR}}, 0},
	{ X(OPER), {{EOR}}, 0},
	{ X(OPERWALL), {{EOR}}, 0},
	{ X(PART), {
			{ CHANNEL, CRLF },
			{ CHANNEL, CHR_SEPERATOR, CHANNEL, CRLF },
			{ EOR }
	}, 3},
	{ X(PASS), {{EOR}}, 0},
	{ X(PING), {{EOR}}, 0},
	{ X(PONG), {{EOR}}, 0},
	{ X(PRIVMSG), {{EOR}}, 0},
	{ X(QUIT), {{EOR}}, 0},
	{ X(REHASH), {{EOR}}, 0},
	{ X(RESTART), {{EOR}}, 0},
	{ X(SERVER), {{EOR}}, 0},
	{ X(SQUIT), {{EOR}}, 0},
	{ X(STATS), {{EOR}}, 0},
	{ X(TIME), {{EOR}}, 0},
	{ X(TOPIC), {{EOR}}, 0},
	{ X(TRACE), {{EOR}}, 0},
	{ X(USER), {{EOR}}, 0},
	{ X(USERHOST), {{EOR}}, 0},
	{ X(VERSION), {{EOR}}, 0},
	{ X(WHO), {{EOR}}, 0},
	{ X(WHOIS), {{EOR}}, 0},
	{ X(WHOWAS), {{EOR}}, 0},
};
#undef X

static inline const char *command_name(enum command_id c)
{
	assert(c < COMMAND_ID_MAX);
	assert(commands[c].name);
	return commands[c].name;
}


static int rand_command_id(void)
{
	return rand() % COMMAND_ID_MAX;
}


static const int *rand_rule_pattern(void)
{
	static int pattern[20];
	unsigned i;

	for (i=0; i < ARRAY_SIZE(pattern) -1 ; i++)
		pattern[i] = (rand() % (EOR - COMMAND_ID_MAX)) + COMMAND_ID_MAX + 1;

	pattern[19] = CRLF;
	return pattern;
}


static const int *rand_command_rule(enum command_id c)
{
	unsigned rows = commands[c].rows;

	if (rows)
		return commands[c].ruleset[rand() % rows];
	return rand_rule_pattern();
}


/* <SPACE>    ::= ' ' { ' ' } */
static int rules_space[][2] = { { EOR }, { CHR_SPACE , EOR } };

/* <params>   ::= <SPACE> [ ':' <trailing> | <middle> <params> ] */
static int rules_params[][4] = {
	{ CHR_SPACE, EOR },
	{ CHR_SPACE, CHR_SEPERATOR, TRAILING, EOR },
	{ CHR_SPACE, MIDDLE, PARAMS, EOR }
};

/* trailing, middle, crlf: handled specially */


/* <target>     ::= <to> [ "," <target> ] */
static int rules_target[][4] = {
	{ TO, EOR },
	{ TO, CHR_COLON, TARGET, EOR }
};


/* <to>         ::= <channel> | <user> '@' <servername> | <nick> | <mask> */
static int rules_to[][4] = {
	{ CHANNEL, EOR },
	{ SYM_USER , CHR_AT, SERVERNAME, EOR },
	{ SYM_NICK, EOR },
	{ MASK, EOR }
};


/* <channel>    ::= ('#' | '&') <chstring> */
static int rules_channel[][3] = {
	{ CHR_HASH, CHSTRING, EOR },
	{ CHR_BANG, CHSTRING, EOR },
	{ CHR_AND, CHSTRING, EOR }
};

/* servername/host: DNS name or IP address in hexadecimal/dotted notation: see rand_host() */

/* <nick> handled specially */
static int rules_mask[][3] = {
	{ CHR_HASH, CHSTRING, EOR },
	{ CHR_DOLLAR, CHSTRING, EOR }
};




static void rand_str(char *buf, size_t max)
{
	size_t len;

	if (max == 0)
		return;

	len = rand() % max;

	buf[len] = 0;
	while (len--) {
		buf[len] = rand() % ('z' - ' ');
		buf[len] += ' ';
	}
}


static inline int rand_char(void)
{
	return rand() & 0xff;
}


static void rand_data(char *buf, size_t max)
{
	size_t len;

	if (max == 0)
		return;

	len = rand() % max;

	buf[len] = 0;
	while (len--)
		buf[len] = rand() & 0xff;
}



static void rand_host(char *buf, size_t max)
{
	size_t len;

	if (max == 0)
		return;

	len = rand();
	if ((len & 1) && max >= 16) {/* dotted-quad */
		snprintf(buf, max, "%d.%d.%d.%d", rand_char(), rand_char(), rand_char(), rand_char());
		return;
	}
	if (len & 2) { /* hex */
		struct sockaddr_in6 s;
		rand_data((char *) &s, sizeof(s));
		s.sin6_family = AF_INET6;
		if (getnameinfo((struct sockaddr *)&s, sizeof(s), buf, max, NULL, 0, NI_NUMERICHOST) == 0)
			return;
	}
	len >>= 2;
	len %= max;
	buf[len] = 0;
	while (len--) {
		buf[len] = rand() % ('z' - 'A');
		buf[len] += 'A';
		if (buf[len] > 'Z' && buf[len] < 'a')
			buf[len] = '.';
	}
}



#if defined(TEST_STANDALONE) || !defined(NDEBUG)
static const char *sym_to_string(int s)
{
	switch (s) {
	case EOR: return "E_O_R";
	case CHANNEL: return "CHANNEL";
	case CHR_AND: return "&";
	case CHR_AT: return "@";
	case CHR_BANG: return "!";
	case CHR_COLON: return ",";
	case CHR_DOLLAR: return "$";
	case CHR_HASH: return "#";
	case CHR_SEPERATOR: return ":";
	case CHR_SPACE: return "SPACE";
	case CHR_MODE: return "CHR_MODE";
	case CHR_NUMBER: return "CHR_NUMBER";
	case CHR_PLUSMINUS: return "CHR_PLUSMINUS";
	case CHSTRING: return "CHSTRING";
	case COMMAND: return "COMMAND";
	case CRLF: return "\\r\\n";
	case HOST: return "HOST";
	case MASK: return "MASK";
	case MESSAGE: return "MESSAGE";
	case MIDDLE: return "MIDDLE";
	case SYM_NICK: return "NICK";
	case PARAMS: return "PARAMS";
	case PREFIX: return "PREFIX";
	case SERVERNAME: return "SERVERNAME";
	case TARGET: return "TARGET";
	case TO: return "TO";
	case TRAILING: return "TRAILING";
	case SYM_USER: return "USER";
	case ADMIN ... WHOWAS:
		return command_name(s);
	}
	printf("invalid symbol %d\n", s);
	abort();
}
#endif

static bool emit(array*a, enum symbols s)
{
	return array_catb(a, (char *) &s, sizeof(s));
}



/*
 * expand symbols recursively until terminal symbol reached.
 * @param:  symbol to expand
 * @param expanded_rule contains ruleset containing only terminal symbols on return
 * @return false if expansion failed (i.e. out of memory, recursion depth exceeded, etc.)
 */
static bool do_expand_symbol(int s, array *expanded_rule, unsigned depth)
{
	const int *rules;
	unsigned rnd = rand();

	if (depth == 0)
		return false;

	switch (s) {
	case CHANNEL:
		rules = rules_channel[rnd % ARRAY_SIZE(rules_channel)];
	break;
	case MASK:
		rules = rules_mask[rnd % ARRAY_SIZE(rules_mask)];
	break;
	case MESSAGE:
		rules = rules_message[rnd % ARRAY_SIZE(rules_message)];
	break;
	case PARAMS:
		rules = rules_params[rnd % ARRAY_SIZE(rules_params)];
	break;
	case PREFIX:
		rules = rules_prefix[rnd % ARRAY_SIZE(rules_prefix)];
	break;
	case TARGET:
		rules = rules_target[rnd % ARRAY_SIZE(rules_target)];
	break;
	case TO:
		rules = rules_to[rnd % ARRAY_SIZE(rules_to)];
	break;
	case CHR_SPACE:
		rules = rules_space[rnd % ARRAY_SIZE(rules_space)];
		if (*rules != EOR)
			break;
		/* else fallthrough: emits SPACE */

	/* Terminal symbols; stop recursing and emit symbol to outbuf */
	case CHR_AND: case CHR_AT: case CHR_BANG:
	case CHR_COLON: case CHR_DOLLAR: case CHR_HASH:
	case CHR_PLUSMINUS: case CHR_SEPERATOR:
	case CHR_MODE: case CHR_NUMBER: case CHSTRING:
	case CRLF: case HOST: case MIDDLE: case SYM_NICK: case SERVERNAME:
	case TRAILING: case SYM_USER: case EOR:

		return emit(expanded_rule, s);
	case COMMAND: /* random command, pick one.... */
		s = rand_command_id();
		/* fallthrough */
	case ADMIN ... WHOWAS:
		rules = rand_command_rule(s);
		/* emit command-symbol ... */
		if (!emit(expanded_rule, s))
			return false;
		/* .. emit  space ... */
		if (!emit(expanded_rule, CHR_SPACE))
			return false;
	break;
	} /* switch () */
	--depth;
	assert(rules);
	/* .. and then start expanding sub rules */
	while (*rules != EOR) {
		if (!do_expand_symbol(*rules, expanded_rule, depth))
			return false;
		rules++;
	}
	return true;
}


static bool expand_symbol(enum symbols s, array *expanded_rule)
{
	bool ret = do_expand_symbol(s, expanded_rule, MAX_RECURSION_DEPTH);
	if (ret)
		return emit(expanded_rule, EOR);
	return false;
}


static bool
catplusminus(array *a)
{
	static const char plus = '+';
	static const char minus = '-';
	return array_catb(a, rand() & 1 ? &plus: &minus, 1);
}

static bool symbols_to_string(int *ruleset, array *res, const char *nick)
{
	bool concat;
	char buf[32];
	char host[64];

	while (*ruleset != EOR) {
		switch (*ruleset) {
		case CHR_AND:
			concat = array_catb(res, "&", 1);
			break;
		case CHR_AT:
			concat = array_catb(res, "@", 1);
			break;
		case CHR_BANG:
			concat = array_catb(res, "!", 1);
			break;
		case CHR_COLON:
			concat = array_catb(res, ",", 1);
			break;
		case CHR_DOLLAR:
			concat = array_catb(res, "$", 1);
			break;
		case CHR_HASH:
			concat = array_catb(res, "#", 1);
			break;
		case CHR_NUMBER:
			sprintf(buf, "%.3d\n", rand() % 1000);
			concat = array_cats(res, buf);
			break;
		case CHR_PLUSMINUS:
			concat = catplusminus(res);
			break;
		case CHR_SEPERATOR:
			concat = array_catb(res, ":", 1);
			break;
		case CHR_SPACE:
			concat = array_catb(res, " ", 1);
			break;
		case HOST:
		case SERVERNAME:
			rand_host(host, sizeof(host));
			concat = array_cats(res, host);
			break;
		case CHR_MODE:
			rand_str(buf, sizeof(buf)/2);
			concat = array_cats(res, buf);
			break;
		case CHSTRING:
		case MIDDLE:
		case SYM_USER:
		case TRAILING:
			rand_str(buf, sizeof(buf));
			concat = array_cats(res, buf);
			break;
		case SYM_NICK:
			concat = array_cats(res, nick);
			break;
		case CRLF:
			return array_catb(res, "\r\n", 3); /* \r\n\0: 3 */
		case ADMIN ... WHOWAS:
			concat = array_cats(res, command_name(*ruleset));
			break;
		default:
#ifndef NDEBUG
		{
			const char *s;
			/* non-terminal symbols: error */
			printf("%s: invalid symbol %d\n", __func__, *ruleset); fflush(NULL);
			s = sym_to_string(*ruleset);
			if (!s) s = "(NULL)";
			printf("%s: invalid symbol %d (%s)\n", __func__, *ruleset, s); fflush(NULL);
			abort();
		}
#endif
			concat = true;
		}
		if (!concat)
			return false;
		ruleset++;
	}
	return true;
}


#ifndef TEST_STANDALONE
static void
add_corruption(array *a, size_t corrupt)
{
	size_t len = array_bytes(a);
	char *b = array_start(a);

	corrupt %= len;

	while (corrupt--) {
		size_t pos = rand();
		pos %= len;
		b[pos] = rand_char();
	}
}


ssize_t irc_fuzz(int fd, const char *nick)
{
	ssize_t br = -1;
	array a, b;

	assert(fd >= 0);

	array_init(&a);
	array_init(&b);
	if (expand_symbol(MESSAGE, &a) &&
		symbols_to_string(array_start(&a), &b, nick))
	{
		size_t len = array_bytes(&b);

		if (!len)
			return -1;
		add_corruption(&b, rand());
		br = irclayer_write(fd, array_start(&b), len);
	}
	array_free(&a);
	array_free(&b);
	return br;
}

#else
#include <time.h>
static void puts_ruleset(int *rule)
{
	while (*rule != EOR) {
		fputs(sym_to_string(*rule), stdout);
		if (*rule == CRLF)
			break;
		rule++;
	}
	puts("");
}


int main(int argc, char *argv[])
{
	enum symbols s = 0;
	array a = INIT_ARRAY;
	array b = INIT_ARRAY;

	srand(time(NULL) ^ getpid());
	if (argc > 1) {
		int x =atoi(argv[1]);
		while (x-- > 0) {
			expand_symbol(MESSAGE, &a);
			fputs("ruleset/symbols: ", stdout);
			puts_ruleset(array_start(&a));
			symbols_to_string(array_start(&a), &b, "nick");
			fputs(array_start(&b), stdout);
			array_trunc(&a);
			array_trunc(&b);
		}
		return 0;
	}

	while (s != EOR) {
		printf("set %d: %s\n", s, sym_to_string(s));
		expand_symbol(s, &a);
		puts_ruleset(array_start(&a));
		array_trunc(&a);
		s++;
	}
	return 0;
}
#endif

