#define LOG_INFO 0
#define LOG_ERR 1
#define LOG_DEBUG 1
#define LOG_WARNING 1

void Log(int Level, const char *Format, ... );
