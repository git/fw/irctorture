/*
 * This file is in the public domain.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "io.h"
#include "async_connect.h"

int async_connect(int fd, const struct sockaddr *serv_addr, socklen_t addrlen)
{
	assert(fd >= 0);

	if (!io_setnonblock(fd))
		return -1;

	return connect(fd, serv_addr, addrlen);
}


int async_connect_status(int fd)
{
	int err, res;
	socklen_t sock_len = sizeof(err);

	res = getsockopt(fd, SOL_SOCKET, SO_ERROR, &err, &sock_len);
	if (res == 0) {
		if (err == 0)
			return 0;
		errno = err;
		return -1;
	}
	return -1;
}

