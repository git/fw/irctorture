#include <sys/types.h>
#include <sys/event.h>

int main(void)
{
	int kfd;
	struct kevent kev;

	kfd = kqueue();
	EV_SET(&kev, 0, EVFILT_READ, EV_ADD|EV_ENABLE, 0, 0, 0);
	return kevent(kfd, &kev, 1, NULL, 0, NULL);
}
