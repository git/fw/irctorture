/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License
 * as published by the Free Software Foundation.
 */
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>

#include "irctorture.h"
#include "array.h"
#include "async_connect.h"
#include "die.h"
#include "fuzzer.h"
#include "io.h"
#include "tls.h"
#include "xfuncs.h"


#define USAGE "usage: irctorture [-vs] [-w max_wakeup_seconds] [-c num_clients] [-i idlecount] [-p port] host"
#define VERBOSE_PRINTF(fmt, ...)	do { if (opts.verbose) printf("debug: " fmt "\n", __VA_ARGS__); } while(0)

struct client {
	char nick[10];
	char ident[10];
	char realname[20];
	time_t wkup_next;	/* next scheduled wakeup (i.e. send random data) */
	time_t conn_next;	/* next scheduled re-connect attempt */
	uint16_t failcnt;
};

static array clients;
static const struct addrinfo *irclayer_server;
static time_t now; /* estimate of current time tick */

static struct {
	pid_t pid;
} env;


static struct {
	int family;
	const char *host;
	const char *port;
	unsigned clientcount;
	unsigned idlecount;
	time_t wakeup_max;
	bool verbose;
	bool ssl;
} opts = { .family = AF_INET, .port = "6667", .clientcount = 1, .wakeup_max = WAKEUP_MAX };


static int spawn_connection(int sock);

static int getseed(void)
{
	int fd = open("/dev/urandom", O_RDONLY);
	int r;
	if (fd < 0)
		die_sys(111,"open");
	read(fd, &r, sizeof(r));
	close(fd);
	return r;
}


static int socket_tcp(void)
{
	int fd = socket(irclayer_server->ai_family, irclayer_server->ai_socktype, irclayer_server->ai_protocol);
	if (fd < 0) {
		perror("socket");
		return -1;
	}
	return fd;
}


static struct client* client_get(int fd)
{
	struct client *c;

	assert(fd >= 0);

	if (fd < 0)
		return NULL;
	c = array_alloc(&clients, sizeof(*c), fd);

	if (!c) die_oom();
	return c;
}


static void usage(void)
{
	die(111, USAGE);
}


static void do_reconnect(int fd)
{
	struct client *c = client_get(fd);
	uint16_t failcount;

	if (c->wkup_next == 0)
		opts.idlecount++;

	failcount = c->failcnt + 1;
	c->failcnt = 0;
	c->wkup_next = 0;

	irclayer_quit(fd);
	tls_shutdown(fd);
	io_close(fd);
	fd = socket_tcp();

	c = client_get(fd);
	failcount *= 2;
	c->failcnt = failcount;
	c->conn_next = now + failcount;
	VERBOSE_PRINTF("%s: reconn (fd %d) , failcnt %u\n", __func__,fd, failcount);
}


static void str_sanitize(char *str)
{
	for (;*str; str++) {
		if (isalnum(*str))
			continue;
		if (isprint(*str))
			continue;
		switch (*str) {
		case '(': case ')': case '[':
		case ':': case ',':
		case ';': case '!':
		case '*': case '@':
		case ' ': break;
		default:
			*str = '_';
		}
	}
}

static void handle_read(int fd)
{
	char buf[1024];
	ssize_t err;
	int i;

	for (i = 0; i < 20; i++) {
		err = irclayer_read(fd, buf, sizeof(buf) -1);
		if (err <= 0)
			break;
		if (opts.verbose) {
			buf[err] = 0;
			str_sanitize(buf);
			printf("client %d: server reply: \"%s\"\n", fd, buf);
		}
	}

	if (i == 20)
		return;

	switch (err) {
	case 0:
		fprintf(stderr, "connection closed on fd %d\n", fd);
		break;
	case IRCERR_SYSTEM:
		fprintf(stderr, "connection closed on fd %d: %s\n", fd, strerror(errno));
		break;
	case IRCERR_WANTWRITE:
		io_event_add(fd, IO_WANTWRITE);
		io_event_del(fd, IO_WANTREAD);
		/* fallthrough */
	case IRCERR_WANTREAD:
		io_event_add(fd, IO_WANTREAD);
		io_event_del(fd, IO_WANTWRITE);
		return;
	default:
		fprintf(stderr, "error on fd %d: %s\n", fd, irclayer_strerror(err));
	}

	do_reconnect(fd);
}


static void handle_write(int fd)
{
	ssize_t err = irclayer_write(fd, NULL, 0);

	if (err > 0)
		return;
	switch (err) {
	case IRCERR_SYSTEM:
		 fprintf(stderr, "write failed on fd %d: %s\n", fd, strerror(errno));
		 break;
	case IRCERR_WANTWRITE:
		io_event_add(fd, IO_WANTWRITE);
		io_event_del(fd, IO_WANTREAD);
		return;
	case 0:	/* fallthrough */
	case IRCERR_WANTREAD:
		io_event_add(fd, IO_WANTREAD);
		io_event_del(fd, IO_WANTWRITE);
		return;
	default:
		fprintf(stderr, "error on fd %d: %s\n", fd, irclayer_strerror(err));
	}
        do_reconnect(fd);
}

static void cb_readwrite(int fd, short event)
{
	if (event & IO_WANTWRITE)
		handle_write(fd);
	handle_read(fd);
}


static void cb_login(int fd, short UNUSED event)
{
	int err;
	struct client *client = client_get(fd);
	assert(client);
	if (!client) {
		fprintf(stderr, "no client structure associated with fd %d?\n", fd);
		io_close(fd);
		return;
	}

	err = irclayer_login(fd, client->nick, client->ident, client->realname);
	switch (err) {
	case 0:
		io_event_setcb(fd, cb_readwrite);

		if (opts.idlecount == 0) {
			if (opts.wakeup_max)
				client->wkup_next = now + (rand() % opts.wakeup_max);
			else	
				client->wkup_next = now;
		} else {
			opts.idlecount--;
		}

		/* fallthrough */
	case IRCERR_WANTREAD:
		io_event_add(fd, IO_WANTREAD);
		io_event_del(fd, IO_WANTWRITE);
		return;
	case IRCERR_SYSTEM:
		fprintf(stderr, "login failed on fd %d: %s\n", fd, strerror(errno));
		break;
	default:
		fprintf(stderr, "login failed on fd %d (nick %s): %s\n", fd, client->nick, irclayer_strerror(err));
		break;
	}
	do_reconnect(fd);
}


static void new_irc_client(int fd)
{
	struct client *c = client_get(fd);

	snprintf(c->nick, sizeof(c->nick), "tst%x%x", fd, env.pid);
	printf("new client %d, nick %s sz %d\n", fd, c->nick, sizeof(c->nick));
	snprintf(c->ident, sizeof(c->ident), "%d", fd);
	strcpy(c->realname, "__GONZO__");
	c->failcnt = 0;
	c->conn_next = 0;

	io_event_setcb(fd, cb_login);
	io_event_add(fd, IO_WANTREAD|IO_WANTWRITE);
}


static void cb_tls_connect(int fd, short UNUSED event)
{
	int err;
	err = tls_connect(fd);

	switch (err) {
	case 0:
		new_irc_client(fd);
		return;
	case TLSERR_WANTWRITE:
		io_event_add(fd, IO_WANTWRITE);
		io_event_del(fd, IO_WANTREAD);
		return;
	case TLSERR_WANTREAD:
		io_event_add(fd, IO_WANTREAD);
		io_event_del(fd, IO_WANTWRITE);
		return;
	default:
		fprintf(stderr, "tls connect failed on fd %d: %s\n", fd, strerror(errno));
	}
	do_reconnect(fd);
}


static void cb_connect(int fd, short UNUSED event)
{
	if (async_connect_status(fd) == 0) {
		if (!opts.ssl) {
			new_irc_client(fd);
			return;
		}
		io_event_setcb(fd, cb_tls_connect);
		return;
	}

	if (errno != EINPROGRESS)
		fprintf(stderr, "connect failed on fd %d: %s\n", fd, strerror(errno));
}


static int lookup_server_tryconnect(void)
{
	int sock = -1;
	struct addrinfo *a, *tmp;
	struct addrinfo hints = {
		.ai_protocol = IPPROTO_TCP,
		.ai_socktype = SOCK_STREAM
	};
	hints.ai_family = opts.family;

	xgetaddrinfo(opts.host, opts.port, &hints, &a);

	for (tmp = a; tmp; tmp = tmp->ai_next) {
		sock = socket(tmp->ai_family, tmp->ai_socktype, tmp->ai_protocol);
		if (sock < 0) {
			perror("socket");
			continue;
		}
		if (connect(sock, tmp->ai_addr, tmp->ai_addrlen) == 0)
			break;
		fprintf(stderr, "connect %s:%s: %s\n", opts.host, opts.port, strerror(errno));
		close(sock);
		sock = -1;
	}
	if (sock == -1)
		die(1, "server unreachable");
	assert(tmp);
	irclayer_server = tmp;
	return sock;
}


static int spawn_connection(int fd)
{
	int err;
	struct client *c = client_get(fd);

	c->conn_next = 0; /* disables reconnects; re-enabled on disconnect */

	err = async_connect(fd, irclayer_server->ai_addr, irclayer_server->ai_addrlen);
	switch (err) {
	case 0:
		if (!io_event_create(fd, IO_WANTWRITE, cb_login))
			die_sys(111, "io_event_create");
		break;
	default:
		if (errno != EINPROGRESS)
			die_sys(111, "connect");

		/* connection is now in progress, tell i/o layer to
		 * call cb_connect when connect() has finished */

		if (!io_event_create(fd, IO_WANTWRITE, cb_connect))
			die_sys(111, "io_event_create");
	}

	if (!irclayer_register(fd)) {
		fprintf(stderr, "could not register fd %d with irc layer\n", fd);
		exit(1);
	}

	return 0;
}


/* open all connections simultaneously, after verifying
 * that we can establish a single connection. */
static void spawn_connections(unsigned num)
{
	unsigned i;
	int fd;

	if (opts.idlecount > num)
		opts.idlecount = num;

	if (!io_library_init(num))
		die_sys(1, "Cannot initialize IO backend");

	fd = lookup_server_tryconnect();

	if (!irclayer_register(fd))
		die_sys(1, "irclayer_register");

	if (opts.ssl) {
		/* first connection uses blocking i/o during connect.
		 * -> no EAGAIN handling needed */
		if (tls_connect(fd)) /* either TLSERR_SYSTEM or 0 */
			die_sys(1, "tls_connect failed");
	}
	if (!io_event_create(fd, IO_WANTWRITE, cb_login))
		die_sys(111, "io_event_create");

	new_irc_client(fd);
	io_setnonblock(fd);
	/* server is reachable, mark connection nonblocking
	 * and open all remaining connections asynchronously */
	for (i = 1; i < num; i++) {
		int err;
		fd = socket_tcp();
		if (fd < 0) {
			if (errno == EMFILE)
				break;
			exit(1);
		}
		err = spawn_connection(fd);
		if (err < 0)
			exit(1);
	}
}


static void mainloop(void)
{
	struct timeval tv = { 1, 0 };
	struct client *c;
	time_t then = now;
	size_t len;


	while (io_dispatch(&tv) >= 0) {
		unsigned i;
		bool reconnect_inprogress = false;

		now = time(NULL);

		if (now == then) /* connection/wakeup checks are done once a second */
			continue;

		len = array_length(&clients, sizeof(*c));
		c = array_start(&clients);
		for (i = 0; i < len; i++) {
			if (c[i].conn_next && c[i].conn_next <= now) {
				/* fd disconnected and retry time reached, reconnect */
				if (!reconnect_inprogress) {
					/* but only start at most one reconnect per iteration */
					reconnect_inprogress = true;
					spawn_connection(i);
				}
				/* client is waiting for reconnect, no wkup check */
				continue;
			}

			if (c[i].wkup_next && c[i].wkup_next <= now) {
				if (irclayer_write(i, NULL, 0) == 0) {
					if (opts.wakeup_max)
						c[i].wkup_next = now + (rand() % opts.wakeup_max);
					else
						c[i].wkup_next = now;
					VERBOSE_PRINTF("client %d: wakeup. next wakeup: %lu seconds", i, (long) c[i].wkup_next - now);
					irc_fuzz(i, c[i].nick);
				} else {
					VERBOSE_PRINTF("client %d: wakeup, had unsent data, skipped", i);
				}
			}
		}
		tv.tv_usec = 0;
		tv.tv_sec =  1;
	}
}


static void parse_args(int argc, char *argv[])
{
	static const char optargs[] = "6hsvw:c:C:p:i:";
	int c, exitcode = 0;
	extern char *optarg;
	extern int optind;

	while ((c = getopt(argc, argv, optargs)) != -1 ) {
		switch (c) {
		case 'v': opts.verbose = true; break;
		case 's': opts.ssl = true; tls_init(); break;
		case 'c': opts.clientcount = (unsigned) xatol(optarg); break;
		case '6': opts.family = AF_INET6; break;
		case 'i': opts.idlecount = (unsigned) xatol(optarg); break;
		case 'w': opts.wakeup_max = (unsigned) xatol(optarg); break;
		default: fprintf(stderr, "Unknown charcode: 0%o\n", c); /* fallthru */
		case '?': exitcode = 1; /* fallthru */
		case 'h': die(exitcode, USAGE);
		case 'p':
			opts.port = optarg;
			break;
		}
	}

	if (!argv[optind])
		usage();
	opts.host = argv[optind];

	if (argv[++optind])
		usage();
}


int main(int argc, char *argv[])
{
	if (argc < 2)
		usage();

	parse_args(argc, argv);
	env.pid = getpid();
	srand(getseed());
	now = time(NULL);

	signal(SIGPIPE, SIG_IGN);

	spawn_connections(opts.clientcount);
	mainloop();

	return 0;
}
