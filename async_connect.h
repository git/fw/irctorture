#include <sys/types.h>
#include <sys/socket.h>

/* calls connect() after making fd O_NONBLOCK */
int async_connect(int fd, const struct sockaddr *serv_addr, socklen_t addrlen);

/* returns 0 if connection has been established, or -1 if not.
 * check errno to determine if connection attempt is still in progress
 * or failure is permanent. see connect(2). */
int async_connect_status(int fd);

