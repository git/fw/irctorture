/*
 * TLS/SSL support.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of version 2 of the GNU General Public License
 * as published by the Free Software Foundation.
 */

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "array.h"
#include "irc.h"
#include "tls.h"

#include "havegnutls.h"

#ifdef HAVE_GNUTLS
#include <gnutls/gnutls.h>

static array tls_sessions;
static gnutls_certificate_credentials_t xcred;

#define TLS_HANDSHAKE_INPROGRESS 1
#define TLS_INTERRUPTED_WRITE 2

struct tls_sessiondata {
	gnutls_session_t gnutls_session;
	unsigned int flags;
};


static bool flag_isset(const struct tls_sessiondata *t, unsigned flag) { return (t->flags & flag) != 0; }
static void flag_clear(struct tls_sessiondata *t, unsigned flag) { t->flags &= ~flag; }
static void flag_set(struct tls_sessiondata *t, unsigned flag) { t->flags |= flag; }



static struct tls_sessiondata *tls_sessiondata_get(int fd)
{
	struct tls_sessiondata *t;
	assert(fd>=0);
	t = array_get(&tls_sessions, sizeof(*t), fd);
	return t;
}


static struct tls_sessiondata *tls_sessiondata_new(int fd)
{
	struct tls_sessiondata *t;
	assert(fd>=0);
	t = array_alloc(&tls_sessions, sizeof(*t), fd);
	return t;
}


static ssize_t gnutls_retval_to_irclayer(struct tls_sessiondata *tls, ssize_t retval, const char *func, bool tried_write)
{
	gnutls_session_t t = tls->gnutls_session;

	switch (retval) {
	case GNUTLS_E_INTERRUPTED:
	case GNUTLS_E_AGAIN:
		if (tried_write)
			flag_set(tls, TLS_INTERRUPTED_WRITE);
		if (gnutls_record_get_direction(t))
			return IRCERR_WANTWRITE;
		return IRCERR_WANTREAD;
	}
	fprintf(stderr, "gnutls_record_%s: %s\n", func, gnutls_strerror(retval));
	return IRCERR_SYSTEM;

}


static ssize_t do_gnutls_read(irclayer_transport_ptr_t t, void *buf, size_t buflen)
{
	int fd = (int) t;
	ssize_t r = -1;
	struct tls_sessiondata *tls = tls_sessiondata_get(fd);

	assert(tls);

	r = gnutls_record_recv(tls->gnutls_session, buf, buflen);

	if (r >= 0)
		return r;
	return gnutls_retval_to_irclayer(tls, r, "recv", false);
}


static ssize_t do_gnutls_write(irclayer_transport_ptr_t t, const void *buf, size_t buflen)
{
	int fd = (int) t;
	ssize_t r = -1;
	struct tls_sessiondata *tls = tls_sessiondata_get(fd);

	assert(tls);

	if (flag_isset(tls, TLS_INTERRUPTED_WRITE)) {
		buflen = 0;
		buf = NULL;
	}
	assert(tls->gnutls_session);
	flag_clear(tls, TLS_INTERRUPTED_WRITE);
	r = gnutls_record_send(tls->gnutls_session, buf, buflen);
	if (r >= 0)
		return r;
	return gnutls_retval_to_irclayer(tls, r, "send", true);
}


static bool tls_register(int fd)
{
	struct tls_sessiondata *t = tls_sessiondata_new(fd);
	if (t) {
		int ret = gnutls_init(&t->gnutls_session, GNUTLS_CLIENT);
		if (ret) {
			fprintf(stderr,"gnutls_init: %s\n", gnutls_strerror(ret));
			return false;
		}

		gnutls_transport_set_ptr(t->gnutls_session, (gnutls_transport_ptr_t) fd);
		ret  = gnutls_set_default_priority(t->gnutls_session);
		if (ret) {
			fprintf(stderr,"gnutls_set_default_priority: %s\n", gnutls_strerror(ret));
			return false;
		}

		ret = gnutls_credentials_set(t->gnutls_session, GNUTLS_CRD_CERTIFICATE, xcred);
		if (ret) {
			fprintf(stderr,"gnutls_credentials_set: %s\n", gnutls_strerror(ret));
			return false;
		}

		flag_clear(t, TLS_INTERRUPTED_WRITE);
		irclayer_transport_set_ptr(fd, (irclayer_transport_ptr_t) fd);
		irclayer_transport_set_push_function(fd, do_gnutls_write);
		irclayer_transport_set_pull_function(fd, do_gnutls_read);
	}
	return t != NULL;
}
#endif /* HAVE_GNUTLS */


int tls_connect(int fd)
{
#ifdef HAVE_GNUTLS
	struct tls_sessiondata *t = tls_sessiondata_get(fd);
	int ret;

	if (!t || !flag_isset(t, TLS_HANDSHAKE_INPROGRESS)) {
		if (!tls_register(fd))
			return TLSERR_SYSTEM;
		t = tls_sessiondata_get(fd);
		assert(t);
		assert(t->gnutls_session);
		flag_set(t, TLS_HANDSHAKE_INPROGRESS);
	}

	assert(t->gnutls_session);
	ret = gnutls_handshake(t->gnutls_session);
	switch (ret) {
	case 0:	/* handshake completed */
		flag_clear(t, TLS_HANDSHAKE_INPROGRESS);
		return 0;
	case GNUTLS_E_AGAIN:
	case GNUTLS_E_INTERRUPTED: /* try again later */
		flag_set(t, TLS_HANDSHAKE_INPROGRESS);
		if (gnutls_record_get_direction(t->gnutls_session))
			return TLSERR_WANTWRITE;
		return TLSERR_WANTREAD;
	default:
		fprintf(stderr, "gnutls_handshake: %s\n", gnutls_strerror(ret));
		errno = EPROTO;
	}
	return TLSERR_SYSTEM;
#else
	errno = ENOSYS;
	return TLSERR_SYSTEM;
#endif
}


void tls_init(void)
{
#ifdef HAVE_GNUTLS
	int ret = gnutls_global_init();
	if (ret) {
		fprintf(stderr,"gnutls_global_init: %s\n", gnutls_strerror(ret));
		exit(1);
	}
	ret = gnutls_certificate_allocate_credentials(&xcred);
	if (ret) {
		fprintf(stderr,"gnutls_certificate_allocate_credentials: %s\n", gnutls_strerror(ret));
		exit(1);
	}
#endif
}


void tls_shutdown(int fd)
{
#ifdef HAVE_GNUTLS
	struct tls_sessiondata *t = tls_sessiondata_get(fd);
	if (t && t->gnutls_session) {
		gnutls_bye(t->gnutls_session, GNUTLS_SHUT_WR);
		gnutls_deinit(t->gnutls_session);
		t->gnutls_session = NULL;
		t->flags = 0;
	}
#else
	(void) fd;
#endif
}
