#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>

/* wrappers around common libc functions. xfuncs
 * always suceed, i.e. will abort the program on failure */

void *xmalloc(size_t size);
long xatol(const char *s);
void xgetaddrinfo(const char *, const char *, struct addrinfo *, struct addrinfo **);
